![输入图片说明](684fd65fc17ce6c103e0d22a5e2e8f5a.png)
#### 介绍
提供一些功能，帮助用户关闭VBS，运行安卓模拟器/VMware必备
<br>
已对家庭版系统进行适配，解决安卓模拟器/虚拟机提示“关闭Hyper-V”的问题

#### 支持系统
Windows10/11 x64（建议Windows10 1903或更高版本），32位系统或其它版本Windows请自测

#### 下载
Releases[点击下载](https://gitee.com/liuying233/disableVBS/releases)


#### 重要提示
关闭VBS后，部分电脑会提示重新设置PIN码。请您在使用本脚本前临时删除PIN码与Windows Hello相关设置

#### **特别鸣谢**
[夜神模拟器](https://www.yeshen.com)
<br>
[bilibili@独醉科技](https://space.bilibili.com/1486110082)